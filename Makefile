include .env
export

rust:
	cd ./handler;\
	cargo lambda build --release

terraform:
	terraform init; \
	terraform plan -out tfplan; \
	terraform apply tfplan

all:
	make rust
	make terraform