
// use lambda_runtime::{ LambdaEvent, Error};
use lambda_http::{run, service_fn, Body, Error, Request, RequestExt, Response};
use serde_json::{json, Value};
use reqwest;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
struct BlogEntrySys {
    id: String,
    publishedAt: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct BlogEntry {
    __typename: String,
    canonicalTitle: String,
    longTitle: String,
    sys: BlogEntrySys,
}

#[derive(Serialize, Deserialize, Debug)]
struct BlogEntryItems {
    items: Vec<BlogEntry>,
}

#[derive(Serialize, Deserialize, Debug)]
struct BlogEntryCollection {
    blogEntryCollection: BlogEntryItems
}

#[derive(Serialize, Deserialize, Debug)]
struct BlogEntryResponse {
    data: BlogEntryCollection
}


async fn get_article_collection() -> Result<Vec<BlogEntry>, reqwest::Error>{

    let contentful_space_id: &'static str = std::env!("CONTENTFUL_SPACE_ID");
    let contentful_env_id: &'static str = env!("CONTENTFUL_ENV_ID");
    let contentful_delivery_token: &'static str = env!("CONTENTFUL_DELIVERY_TOKEN");

    let url = format!("https://graphql.contentful.com/content/v1/spaces/{}/environments/{}", contentful_space_id, contentful_env_id);
    let auth_token =  format!("Bearer {}", contentful_delivery_token);

    let request_body = json!({"query":
        "query {
            blogEntryCollection(order: longTitle_ASC) {
                items {
                    __typename
                    longTitle
                    canonicalTitle
                    sys {
                        id
                        publishedAt
                    }
                }
            }
        }"
    }).to_string();


    let client = reqwest::Client::new();

    let response = client.post(url)
        .header("Authorization", auth_token)
        .header("Content-Type", "application/json")
        .body(request_body)
        .send()
        .await?
        .json::<BlogEntryResponse>()
        .await;

    return Ok(response.unwrap().data.blogEntryCollection.items);

}

fn generate_site_map(articles: Vec<BlogEntry>) -> String {
    let mut site_map = String::from("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    site_map.push_str("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");

    for article in articles {
        let url = format!("https://ben.website/{}", article.canonicalTitle);
        site_map.push_str(&format!("<url><loc>{}</loc><lastmod>{}</lastmod></url>\n", url, article.sys.publishedAt));
    }

    site_map.push_str("</urlset>");
    return site_map;
}

async fn handler(_event: Request) -> Result<Response<String>, Error>  {
    let articles = get_article_collection().await;
    let sitemap = generate_site_map(articles.unwrap());
    println!("{:?}", sitemap);

    let resp = Response::builder()
        .status(200)
        .header("Content-Type", "application/xml")
        .body(sitemap)
        .expect("failed to render response");

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    run(service_fn(handler)).await
}